package com.example.myapplication;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CallLog extends RealmObject {


    private String phone_no;
    private String customer_no;
    private String call_type;
    private String duration;

    @PrimaryKey
    private Long time;

    private boolean sync = false;


    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public void setCustomer_no(String customer_no) {
        this.customer_no = customer_no;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public void setTime(Long time) {
        this.time = time;
    }


    public String getPhone_no() {
        return phone_no;
    }

    public String getCustomer_no() {
        return customer_no;
    }

    public String getDuration() {
        return duration;
    }

    public Long getTime() {
        return time;
    }

    public String getCall_type() {
        return call_type;
    }

    public void setCall_type(String call_type) {
        this.call_type = call_type;
    }

    public boolean isSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }
}
