package com.example.myapplication;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.CallLog;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    Context c;

    private static final String TAG = "CallLog";
    private static final int URL_LOADER = 1;

    public static String phoneNumber = null;

    private TextView callLogsTextView;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate()");

        setContentView(R.layout.activity_main);
        initialize();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("CutPasteId")
    private void initialize() {
        Log.d(TAG, "initialize()");

        callLogsTextView = (TextView) findViewById(R.id.call_logs);

        Log.d(TAG, "initialize() >> initialise loader");
        if (isPermissionGranted()) {
            getLoaderManager().initLoader(URL_LOADER, null, MainActivity.this);

            TelephonyManager mTelephonyMgr;
            mTelephonyMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);


            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            phoneNumber = mTelephonyMgr.getSimSerialNumber();
        }

        // System.out.println("phoneNumber" +phoneNumber );

    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderID, Bundle args) {
        Log.d(TAG, "onCreateLoader() >> loaderID : " + loaderID);

        switch (loaderID) {
            case URL_LOADER:
                // Returns a new CursorLoader
                return new CursorLoader(
                        this,   // Parent activity context
                        CallLog.Calls.CONTENT_URI,        // Table to query
                        null,     // Projection to return
                        null,            // No selection clause
                        null,            // No selection arguments
                        null             // Default sort order
                );
            default:
                return null;
        }

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor managedCursor) {
        Log.d(TAG, "onLoadFinished()");
        Realm realm = Realm.getDefaultInstance();
        StringBuilder sb = new StringBuilder();

        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);

        sb.append("<h4>Call Log Details <h4>");
        sb.append("\n");
        sb.append("\n");

        sb.append("<table>");

        List<com.example.myapplication.CallLog> list = new ArrayList<>();

        while (managedCursor.moveToNext()) {

            String phNumber = managedCursor.getString(number);
            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            // Date callDayTime = new Date(Long.valueOf(callDate));

            String callDayTime;
            callDayTime = DateFormat.format("HH:MM", Long.parseLong(callDate)).toString();

            // String callDayTime = dateFormat.format(callDate);  
            String callDuration = managedCursor.getString(duration);
            String dir = null;

            int callTypeCode = Integer.parseInt(callType);
            switch (callTypeCode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "Outgoing";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "Incoming";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "Missed";
                    break;
            }

            if (realm.where(com.example.myapplication.CallLog.class).equalTo("time", Long.parseLong(callDate)).findFirst() == null) {

                com.example.myapplication.CallLog callLog = new com.example.myapplication.CallLog();
                callLog.setCustomer_no(phoneNumber);
                callLog.setDuration(callDuration);
                callLog.setPhone_no(phNumber);
                callLog.setCall_type(dir);
                callLog.setTime(Long.parseLong(callDate));
                list.add(callLog);
            }

            sb.append("<tr>")
                    .append("<td>Phone Number: </td>")
                    .append("<td><strong>")
                    .append(phNumber)
                    .append("</strong></td>");
            sb.append("</tr>");
            sb.append("<br/>");
            sb.append("<tr>")
                    .append("<td>Call Type:</td>")
                    .append("<td><strong>")
                    .append(dir)
                    .append("</strong></td>");
            sb.append("</tr>");
            sb.append("<br/>");
            sb.append("<tr>")
                    .append("<td>Date & Time:</td>")
                    .append("<td><strong>")
                    .append(callDayTime)
                    .append("</strong></td>");
            sb.append("</tr>");
            sb.append("<br/>");
            sb.append("<tr>")
                    .append("<td>Call Duration (Seconds):</td>")
                    .append("<td><strong>")
                    .append(callDuration)
                    .append("</strong></td>");
            sb.append("</tr>");
            sb.append("<br/>");
            sb.append("<br/>");
        }
        sb.append("</table>");

        managedCursor.close();


        if (!list.isEmpty()) {
            realm.beginTransaction();
            try {
                realm.copyToRealmOrUpdate(list);
                realm.commitTransaction();
            } catch (Exception e) {
                e.printStackTrace();
                realm.cancelTransaction();
            }
        }

        System.out.println("Start Printing");
        for (com.example.myapplication.CallLog c : realm.where(com.example.myapplication.CallLog.class).equalTo("sync", false).findAll()) {
            System.out.println(" calllog  ___ " + c.getTime() + "  " + c.toString());
            Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();

            APIApplication apiApplication = retrofit.create(APIApplication.class);
            callAPI(apiApplication, c);
        }

        callLogsTextView.setText(Html.fromHtml(sb.toString()));
    }

    private void callAPI(APIApplication apiApplication, final com.example.myapplication.CallLog log) {
        apiApplication.postLog("11", log.getPhone_no(), log.getCustomer_no(), log.getDuration(), log.getTime() + "")
                .enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        try {
                            Realm realm = Realm.getDefaultInstance();
                            realm.beginTransaction();

                            log.setSync(true);
                            realm.commitTransaction();
                            realm.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        t.printStackTrace();
                    }
                });


    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.d(TAG, "onLoaderReset()");
        // do nothing
    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.READ_CALL_LOG)
                    == PackageManager.PERMISSION_GRANTED && checkSelfPermission(android.Manifest.permission.READ_PHONE_STATE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {

                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_CALL_LOG, Manifest.permission.READ_CALL_LOG}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {

            case 1: {

                if (grantResults.length > 0
                        && checkSelfPermission(android.Manifest.permission.READ_CALL_LOG)
                        == PackageManager.PERMISSION_GRANTED) {
                    TelephonyManager mTelephonyMgr;
                    mTelephonyMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

                    if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    phoneNumber = mTelephonyMgr.getSimSerialNumber();
                    getLoaderManager().initLoader(URL_LOADER, null, MainActivity.this);

                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
