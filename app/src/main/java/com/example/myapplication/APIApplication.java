package com.example.myapplication;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface APIApplication {

    @FormUrlEncoded
    @POST("Api.php?apicall=call_log")
    Call<Void> postLog(@Field("ids") String ids,
                               @Field("phone_no") String phone_no,
                               @Field("customer_no") String customer_no,
                               @Field("duration") String duration,
                               @Field("time") String time);
}
